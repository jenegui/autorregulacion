$(document).ready(function () {
    function validateEmail(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function algunoVacio() {
        var term1 = $("#name").val();
        var term2 = $("#correo").val();
        var term3 = $("#telefono").val();
        var term4 = $("#check1").prop('checked');//true false
        if (term1 == '' || term2 == '' || term3 == '' ||  term4 == false) {
            return true;
        } else {
            return false;
        }
    }

    function telefono() {
        var tele = $("#telefono").val().length;
        if (tele < 6) {
            //swal("Error", "El teléfono debe contener mínimo 6  caractéres.", "error");
            return true;
        } else if(tele > 16) {
            //swal("Error", "El teléfono debe contener máximo 6  caractéres.", "error");
            return true;
        } else {
            return false;
        }
    }

    function tipoEstab() {
        var tipo = $("#tipo").val();
        if (tipo == "-") {
            
            return true;
        
        } else {
            return false;
        }
    }

    //$(".faqs").niceScroll(".faq_wrapper", { cursorwidth: "6px",autohidemode: false });
    //$(".verifica-lista").niceScroll(".verifica-lista-wrapper", { cursorwidth: "6px",autohidemode: false });

    $('.collapse').on('shown.bs.collapse', function () {
        //$(this).prev().addClass('active-acc');
        $(this).parent().addClass('active-acc');
        //$(".faqs").getNiceScroll().resize();
    });

    $('.collapse').on('hidden.bs.collapse', function () {
        //$(this).prev().removeClass('active-acc');
        $(this).parent().removeClass('active-acc');
    });


    $(".faqs-btn button").click(function(){
        //if( $("#autocomplete").val()=="" ){
            $(".faqs .card").show();
        //}
    });

    $("#btn_submit").click(function(){
        var email = $("#correo").val();
        if (validateEmail(email)) {
            if (!algunoVacio()) {
                if(!telefono()){
                    if(!tipoEstab()){
                        return true;
                    }else{
                        swal("Error", "Todos los campos son obligatorios.", "error");
                        return false;
                    }
                }else{
                    swal("Error", "El número de teléfono no es válido.", "error");
                    return false;
                }
            } else {
                swal("Error", "Todos los campos son obligatorios.", "error");
                return false;
            }
        } else {
            swal("Error", "Formato de correo no válido.", "error");
            return false;
        }

    });
    

});

