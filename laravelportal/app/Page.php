<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Page extends Model
{
    // Fetch all users
   public static function getUsers(){

     $records = DB::table('newsletters')
     				->join('tipos_de_establecimientos', 'tipos_de_establecimientos.id', '=', 'newsletters.id_tipo_establecimiento')
     				->select('tipos_de_establecimientos.id','tipos_de_establecimientos.nombre_tipo_de_establecimiento','newsletters.name','newsletters.correo_e','newsletters.telefono','newsletters.created_at')
     				->orderBy('id', 'asc')->get()->toArray();

     return $records;
   }
}
