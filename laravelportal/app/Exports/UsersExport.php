<?php

namespace App\Exports;

use App\Page;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;


class UsersExport implements FromCollection,WithHeadings,WithColumnFormatting
{
    /**
    * @return \Illuminate\Support\Collection
    */
    /*public function collection()
    {
        
        return Newsletter::join('tipos_de_establecimientos','id_tipo_establecimiento','=','tipos_de_establecimientos.id')->get();
        

    }*/
    public function columnFormats(): array
    {
        return [
              'A' => '',
            'B' => '',
            'C' => '',
            'D' => '',
            'E' => '',		
             'F' => 'dd-mm-yyyy ',
        ];
    }

    public function headings(): array {
	    return [
	       'id','Establecimiento','Nombre propietario','Correo','Teléfono','Fecha_registro'
	    ];
	  }



	  /**
	  * @return \Illuminate\Support\Collection
	  */
	  public function collection() {

	     return collect(Page::getUsers());
	      //return Page::getUsers(); // Use this if you return data from Model without using toArray().
	  }

    
}
