<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
    use Illuminate\Foundation\Validation\ValidatesRequests;
    use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\Auth;
    use Illuminate\Support\Facades\DB;

use App\Models\{Entrada,Establecimiento,Newsletter,Noticia,Tipos_de_establecimiento};
use App\User;
use App\Exports\UsersExport;
use Maatwebsite\Excel\Facades\Excel;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    function home(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return view('admin.login',['logged'=>null]);
        } else{
            return redirect(url('admin/stats'));
        }
    }

    function checklogin(Request $request){
        $nombre=$request->input("nombre",null);
        $password=$request->input("clave",null);

        if(Auth::attempt(['name'=>$nombre,'password'=>$password]) ){
            $request->session()->put('admin','1');
            return redirect(url('admin/stats'));
        }else{
            return view('admin.login',['logged'=>null]);
        }
    }
    function logout(Request $request){
        $request->session()->forget('admin');
        return redirect(url('admin/'));
    }



    function stats(Request $request){
        $logged=$request->session()->get('admin',null);

        if($logged == null){
            return redirect(url('admin/'));
        } 

        $count=NewsLetter::all()->count();
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.stats',['tipos'=>$tipos,'cantidad'=>$count,'logged'=>'Admin']);
    }




    function newsletter(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $results=Newsletter::with('establecimiento')->paginate(20);
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.newsletter',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
    }
    function searchnewsletter(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $param=$request->input("filter",null);
        $term=$request->input("term",null);
        if($param==null||$term==null){
            return redirect(url('/admin/newsletter'));
        }else{
            if($param==1){
                $results=Newsletter::with('establecimiento')->where('name','like',"%$term%")->paginate(50);
            }else {
                $results=Newsletter::with('establecimiento')->where('correo_e','like',"%$term%")->paginate(50);
            }
            $tipos=DB::table('tipos_de_entradas')->get();
            return view('admin.newsletter',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
        }
    }















    function entradas($id,Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 

        $results=Entrada::where('tipo',$id)->paginate(20);
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.entradas',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
    }
    function searchentrada(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $term=$request->input("term",null);
        if($term==null){
            return redirect(url('/admin/entradas'));
        }else{
            $results=Entrada::where('contenido','like',"%$term%")->paginate(50);
        }
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.entradas',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
    }
    function editEntrada($id,Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $result=Entrada::find($id);
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.editEntrada',['tipos'=>$tipos,'result'=>$result,'logged'=>'Admin']);
    }
    function newEntrada(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $result=new Entrada();
        $result->enunciado="";
        $result->contenido="";
        $result->tipo="";
        $result->imagen="";
        $result->estado="0";
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.editEntrada',['tipos'=>$tipos,'result'=>$result,'logged'=>'Admin']);
    }
    function saveEntrada(Request $request){
        $idn=$request->input('ide',"");
        $enunciado=$request->input('enunciado',"");
        $tipo=$request->input('tipo',"");
        $contenido=$request->input('contenido',"");
        if($idn==""){
            //nuevo registro
            $entrada=new Entrada();
            $entrada->enunciado=$enunciado;
            $entrada->tipo=$tipo;
            $entrada->contenido=$contenido;
            $entrada->estado=1;
            if($request->hasFile('archivo')){
                //$path=$request->archivo->store('public');
                //dd($request->archivo);
                $path = $request->archivo->storeAs('public',$request->archivo->getClientOriginalName() );
                $entrada->archivo="storage/".substr($path,7);
            }
            $entrada->save();
        }else{
            //registro actualizable
            $entrada=Entrada::find($idn);
            $entrada->enunciado=$enunciado;
            $entrada->tipo=$tipo;
            $entrada->contenido=$contenido;
            if($request->hasFile('archivo')){
                //dd($request->archivo);
                //$path=$request->archivo->store('public');
                $path = $request->archivo->storeAs('public',$request->archivo->getClientOriginalName() );
                $entrada->archivo="storage/".substr($path,7);
            }
            $entrada->save();
        }
        return redirect(url('admin/entradas'));
    }
    function toogleEntrada($id,Request $request){
        $entrada=Entrada::find($id);
        if($entrada->estado==1){
            $entrada->estado=0;
        }
        else {
            $entrada->estado=1;
        }
        $entrada->save();
        return redirect(url('admin/editentrada/'.$id));
    }























    function noticias(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $results=Noticia::paginate(20);
         $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.noticias',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
    }
    function searchnews(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $term=$request->input("term",null);
        if($term==null){
            return redirect(url('/admin/noticias'));
        }else{
            $results=Noticia::where('titulo','like',"%$term%")->paginate(50);
             $tipos=DB::table('tipos_de_entradas')->get();
            return view('admin.noticias',['tipos'=>$tipos,'results'=>$results,'logged'=>'Admin']);
        }
    }

    function editNoticia($id,Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $tipos=DB::table('tipos_de_entradas')->get();
        $result=Noticia::find($id); //['id'=>"",'titulo'=>"",'resumen'=>"",'contenido'=>"",'imagen'=>"",'estado'=>""];
        return view('admin.editNoticia',['tipos'=>$tipos,'result'=>$result,'logged'=>'Admin']);
    }

    function newNoticia(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        } 
        $result=new Noticia();
        $result->titulo="";
        $result->resumen="";
        $result->contenido="";
        $result->imagen="";
        $result->estado="0";
        $tipos=DB::table('tipos_de_entradas')->get();
        return view('admin.editNoticia',['tipos'=>$tipos,'result'=>$result,'logged'=>'Admin']);
    }
    function saveNoticia(Request $request){
        $idn=$request->input('idn',"");
        $titulo=$request->input('titulo',"");
        $resumen=$request->input('resumen',"");
        $contenido=$request->input('contenido',"");
        $piefoto=$request->input('piefoto',"");
        if($idn==""){
            //nuevo registro
            $noticia=new Noticia();
            $noticia->titulo=$titulo;
            $noticia->resumen=$resumen;
            $noticia->contenido=$contenido;
            $noticia->piefoto=$piefoto;
            $noticia->estado=1;
            if($request->hasFile('imagen')){
                $path=$request->imagen->store('public');
                $noticia->imagen="storage/".substr($path,7);
            }
            
            $noticia->save();
        }else{
            //registro actualizable
            $noticia=Noticia::find($idn);
            $noticia->titulo=$titulo;
            $noticia->resumen=$resumen;
            $noticia->contenido=$contenido;
            $noticia->piefoto=$piefoto;
            if($request->hasFile('imagen')){
                $path=$request->imagen->store('public');
                $noticia->imagen="storage/".substr($path,7);
            }
            $noticia->save();
        }
        return redirect(url('admin/noticias'));
    }

    function toogleNoticia($id,Request $request){
        $noticia=Noticia::find($id);
        if($noticia->estado==1){
            $noticia->estado=0;
        }
        else {
            $noticia->estado=1;
        }
        $noticia->save();
        return redirect(url('admin/editnoticia/'.$id));
    }

    function download(Request $request){
        $logged=$request->session()->get('admin',null);
        if($logged == null){
            return redirect(url('admin/'));
        }

        return Excel::download(new UsersExport, 'newsletters.xlsx');
    }
}