<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    //
    public function establecimiento(){
        return $this->belongsTo(Tipos_de_establecimiento::class,'id_tipo_establecimiento');
    }
}
