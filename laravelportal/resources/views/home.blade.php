
@extends('layout', ['title' => 'Home','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'logos'=>$logos ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
            </ul>      
        </div>
        @isset($msg)
        <div class="alert alert-primary" role="alert">
            {{$msg}}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endisset
        <div class="main_slider">
            <div id="homeSlider" class="carousel slide" data-ride="carousel">
                
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        @if ($slide->contenido=="decreto087")
                            <div class="carousel-item {{$activo}}">
                                <div class="container1">
                                    <a href="https://drive.google.com/file/d/1OFuVGTUb6p8oXC_Jwq2yBZwFfWpizBq-/view?usp=sharing">
                                        <img class="w-100" src="{{$slide->archivo}}" alt="">
                                    </a>
                                </div>
                            </div>
                        @elseif ($slide->contenido=="controlplagas")
                            <div class="carousel-item">
                                <div class="container1">
                                    <a href="https://drive.google.com/open?id=1h9yUg0aK5K9__KXJE6uzrkCFTIW-OXMw">
                                        <img class="w-100" src="{{$slide->archivo}}" alt="">
                                    </a>
                                </div>
                            </div>
                        @elseif ($slide->contenido=="carnicos")
                            <div class="carousel-item">
                                <div class="container1">
                                    <a href="https://forms.gle/ZqU7Kab6AUB1ffQv8">
                                        <img class="w-100" src="{{$slide->archivo}}" alt="">
                                    </a>
                                </div>
                            </div>    
                        @else
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @endif
                        @php
                            $activo="";
                        @endphp
                    @endforeach

                </div>
                <br>
               
                <ol class="carousel-indicators">
                     <li style="display: none">
                     </li>
                     <li style="display: none">
                    </li>
                    <li style="display: none">
                    </li>
                    <li>
                        <div data-target="#homeSlider" data-slide-to="3" >
                            <div class="logo logo_rest"></div>
                            <div class="text">Restaurantes</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li>
                        <div data-target="#homeSlider" data-slide-to="4" >
                            <div class="logo logo_carne"></div>
                            <div class="text">Expendios de Carne</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li>
                        <div data-target="#homeSlider" data-slide-to="5" >
                            <div class="logo logo_drog"></div>
                            <div class="text">Droguerías</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                    <li>
                        <div data-target="#homeSlider" data-slide-to="6" >
                            <div class="logo logo_salon"></div>
                            <div class="text">Salones de belleza</div>
                        </div>
                        <div class="text-right mt-2">
                            <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                        </div>
                    </li>
                </ol>

                <a class="carousel-control-prev" href="#homeSlider" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#homeSlider" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>

        <div class="row block right no-gutters">
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 mx-auto" src="{{url('/'.$bloque1->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!!$bloque1->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque1->contenido!!}
                </div>
                <div class="text-center">
                    <a href="{{url('/'.$instructivo->archivo)}}" target="blank"><button class="btn yellow">{!!$instructivo->enunciado!!}</button></a>
                     <a href="https://drive.google.com/file/d/17LHFsBf1MHmjIP1aVMz3GcDW4q8W4UxD/view" target="blank"><button class="btn yellow">{!!$videotutorial->enunciado!!}</button></a><br><br> 
                    <!--<a  href="{{ 'download' }}" target="blank"><button class="btn yellow">Guia para registro y visita</button></a>-->
                    <a href="{{$bloque2->contenido}}" target="blank"><button class="btn yellow">{!!$bloque2->enunciado!!}</button></a>
                </div>
                <br>
                <!--JENEGUI-->
                <div class="text-right">
                    
                </div>
            </div>
        </div>


        <div class="row block right no-gutters">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2>{!!$bloque3->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque3->contenido!!}
                </div>
                <div class="text-left">
                    <button class="btn yellow" data-toggle="modal" data-target="#consulta">Consulta aquí</button>
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-80 mx-auto" src="{!!$bloque3->archivo!!}" alt="alternative text">
            </div>
        </div>


    <!-- Modal -->
    <div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="consultaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <div class="subtitle"><b>Consulte ahora los negocios con concepto favorable</b></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body newsletter">
            <div class="my-4">
                <h3 class="text-blue">Filtros:</h3>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">Tipos de Establecimiento</label>
                    <select  name="" id="testablecimiento">
                        <option value="" selected>ninguno</option>
                        @foreach ($te as $t)
                            <option value="{{$t->id}}">{{$t->nombre_tipo_de_establecimiento}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">Localidad</label>
                    <select  name="" id="localidad">
                        <option value="" selected>ninguno</option>
                        @foreach ($localidades as $loc)
                            <option value="{{$loc->codLocalidad}}">{{$loc->NombreLocalidad}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">UPZ</label>
                    <select  name="" id="upz">
                        <option value="" selected>ninguno</option>
                        @foreach ($upzs as $upz)
                            <option value="{{$upz->codUPZ}}">{{$upz->NombreUPZ}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">Barrio</label>
                    <select  name="" id="barrio">
                        <option value="" selected>ninguno</option>
                        @foreach ($barrios as $ba)
                            <option value="{{$ba->CodigoBarrio}}">{{$ba->NombreBarrio}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="right-content w-100 my-2">
                <input id="term" class="form-control mr-sm-2" type="text" placeholder="Nombre Comercial" aria-label="Nombre Comercial">
            </div>
            <button id="btn_consultar" class="btn yellow mb-4">Consultar</button>
            <button id="btn_nueva_consulta" class="btn yellow mb-4 ml-4">Nueva consulta</button>
            <table id="modalTable" class="table table-striped table-bordered w-100">
               <thead>
                    <tr>
                        <th>Nombre comercial</th>
                        <th>Dirección comercial</th>
                        <th>Fecha de visita</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

@endsection


@section("redes")
    <main class="container">
        <div class="row block redes">
            <div class="col-12 col-md-7">
                {{--<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLaHEra_rXLyV9NBLjpMAfcEnyxsxyKvHl" frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>--}}
                    
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/2IpydNh5kMc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
            </div>
            <div class="col-12 col-md-5">
                <div class="twitter-feed"><!-- 
                    <iframe src="http://twubs.com/embed/autorregulación/?messagesPerPage=5&headerBgColor=%231c6485&headerTextColor=%23ffffff" width="100%" height="515" frameborder="0"><a href="http://twubs.com/autorregulación">#autorregulación</a></iframe>

                    <a href="https://twitter.com/intent/tweet?button_hashtag=Autorregulación&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-show-count="false">Tweet #Autorregulación</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
 -->
                    
                    <a class="twitter-timeline" data-height="500" href="https://twitter.com/SectorSalud?ref_src=twsrc%5Etfw">Tweets by SectorSalud</a>
                    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    
                    <!-- <a target="_blank" href="https://twitter.com/search?q=%23NegociosSaludablesNegociosRentables">#Autorregulación</a> -->

                    <a href="https://twitter.com/intent/tweet?button_hashtag=NegociosSaludablesNegociosRentables&text=@sectorsalud&ref_src=twsrc%5Etfw" class="twitter-hashtag-button" data-show-count="false">Tweet #Autorregulación</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                </div>
                
            </div>
        </div>
    </main>
@endsection


@section ("scripts")
    <script languague="javascript">      

    $(document).ready(function () {
        

        $("#btn_nueva_consulta").click(function(){
            $("#testablecimiento").val("");
            $("#localidad").val("");
            $("#barrio").val("");
            $("#upz").val("");
            $("#term").val("");
            $("#modalTable").DataTable().clear().draw(false);
        });
        $("#btn_consultar").click(function(){
            //$("#modalTable").html("");
            var theval=$("#term").val();

            var tf1=$("#testablecimiento").val();
            var tf2=$("#localidad").val();
            var tf3=$("#barrio").val();
            var tf4=$("#upz").val();

            $.post("{{url('/consultar')}}",{'term':theval.toString(),'tf1':tf1,'tf2':tf2,'tf3':tf3,'tf4':tf4,'_token':'{{ csrf_token() }}'},function(data){
                console.log(data);
                if(data=="error"){
                    //$("#modalTable").html("No hay resultados");
                    $("#modalTable").DataTable().clear().draw(false);
                }else{
                    var jdata=JSON.parse(data);
                    $("#modalTable").DataTable().clear();
                    //var tabla="<thead><tr><th>Nombre Comercial</th><th>Razón Social</th></tr></thead>";
                    var tabla="";
                    jQuery.each(jdata,function(i,val){
                        tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.DireccionComercial+"</td><td>"+val.FechaVisita+"</td></tr>";
                        $("#modalTable").DataTable().row.add($(tabla));
                    });
                    
                    //$("#modalTable").html(tabla);
                   
                    $("#modalTable").DataTable().draw();
                }
            });
        });
        $("#localidad").on('change',function(){
            var tf=$("#localidad").val();
            $.get("{{url('/filtroloc')}}",{'localidad':tf},function(data){
                console.log(data);
                if(data=="error"){
                    // no hago nada
                }else{
                    $("#upz").html("");
                    var jdata=JSON.parse(data);
                    var html="<option value='' selected>ninguno</option>";
                    jQuery.each(jdata,function(i,val){
                        html+="<option value='"+val.codUPZ+"' selected>"+val.NombreUPZ+"</option>";
                    });
                    $("#upz").html(html);
                }
            });
        });
        $("#upz").on('change',function(){
            var tf=$("#upz").val();
            $.get("{{url('/filtroupz')}}",{'upz':tf},function(data){
                console.log(data);
                if(data=="error"){
                    // no hago nada
                }else{
                    $("#barrio").html("");
                    var jdata=JSON.parse(data);
                    var html="<option value='' selected>ninguno</option>";
                    jQuery.each(jdata,function(i,val){
                        html+="<option value='"+val.CodigoBarrio+"' selected>"+val.NombreBarrio+"</option>";
                    });
                    $("#barrio").html(html);
                }
            });
        });


        $('#modalTable').DataTable( {
            searching: false,
            "pageLength": 5,
            "language": {
                "lengthMenu": "Mostrar _MENU_ Registros por página",
                "zeroRecords": "No hay resultados.",
                "info": "Mostrando página _PAGE_ de _PAGES_",
                "infoEmpty": "No hay registros disponibles",
                "infoFiltered": "(filtered from _MAX_ total records)",
                "paginate": {
                    "first":      "Primero",
                    "previous":   "Anterior",
                    "next":       "Siguiente",
                    "last":       "Último"
                },
            }
        });
        
        @isset($msg)
            swal("Suscripción exitosa", "Hemos recibido tus datos, recibirás información actualizada sobre tu tipo de negocio en el correo electrónico.", "success");
        @endisset

        $('.carousel').carousel({
          interval: 3000
        });
        .carousel('prev')
        
    });
    </script>
@endsection