@extends('layout',['title' => 'BuenPropietario','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'logos'=>$logos])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/buenpropietario')}}">Sea un buen propietario</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @php
                            $activo="";
                        @endphp
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @php
                        $activo="active";
                        $i=0;
                    @endphp
                    @foreach($sliders as $slide)
                        <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                            <span></span>                        
                        </li>
                        @php
                            $activo="";
                            $i++;
                        @endphp
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center py-4">
                <img class="w-80 m-auto my-5" src="{{url('/'.$bloque1->archivo)}}" alt="{{ $bloque1->enunciado }}">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!! $bloque1->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque1->contenido !!}
                </div>
            </div>
        </div>



        <div class="row block right my-5">
            <div class="col-12 py-4">
                <div class="subtitle">
                    <b>Sea un</b> buen propietario
                </div>
                <div class="recomendaciones">
                    
                    @foreach($buenos as $b)
                        <div><div class="row">
                            <div class="col-12 col-md-2">
                                <img src="{{$b->archivo}}" alt="">
                            </div>
                            <div class="col-12 col-md-10">
                                <h3 class="text-blue"><b>{{$b->enunciado}}</b></h3>
                                <p>
                                {!!$b->contenido!!}
                                </p>
                            </div>
                        </div></div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="row justify-content-between">
            <div class="block col-12 col-md-5 px-2">
                <div class="subtitle">
                    <h2>{!! $bloque2->enunciado !!}</h2>
                </div>
                <p>
                    {!! $bloque2->contenido !!}
                </p>
            </div>
            <div class="block col-12 col-md-6 py-5">
                <div id="ejemploSlider" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @php
                            $active="active";
                        @endphp
                        @foreach($ejemplos as $ej)
                            
                            <div class="carousel-item {{$active}}">
                                <img src="{{$ej->archivo}}" alt="">
                                <div class="text-blue mt-4"><h3><b>{{$ej->enunciado}}</b></h3></div>
                                <div>{!!$ej->contenido!!} </div>
                            </div>
                            @php
                                $active="";
                            @endphp
                        @endforeach
                        
                    </div>
                    <ol class="carousel-indicators">
                        <li data-target="#ejemploSlider" data-slide-to="0" class="active">
                            <span></span>                        
                        </li>
                        <li data-target="#ejemploSlider" data-slide-to="1">
                            <span></span>
                        </li>
                        <li data-target="#ejemploSlider" data-slide-to="2">
                            <span></span>
                        </li>
                    </ol>
                </div>
            </div>
        </div>



        <div class="row my-5" id="estrategiasexcelencia">
            <div class="subtitle">
                <h1>{!! $bloque3->enunciado !!}</h1>
            </div>
            <p>{!! $bloque3->contenido !!}</p>
            <div class="estrategias">
                @php
                    $i=1;
                @endphp
                @foreach($stra as $st)
                    <div class="text-center">
                        <img src="{{$st->archivo}}" alt="{{$st->enunciado}}" title="{{$st->enunciado}}">
                        <h5 class="text-blue"><b>{{$st->enunciado}}:</b></h5>
                        <button class="btn yellow" data-toggle="modal" data-target="#estrategia{{$i}}">Ver más</button>
                        {{--<button class="btn yellow">Ver video</button>--}}
                    </div>
                    @php
                        $i++;
                    @endphp
                @endforeach
               
            </div>
        </div>



        <div class="row block my-5">
            <div class="col-12 col-md-12">
                <div class="subtitle">
                    <h2><b>Documentos</b></h2>
                </div>
                <div class="normativas">
                    @foreach($docs as $doc)
                        <div class="normativa">
                            <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                            <div class="content">
                                <h3 class="text-blue">{{$doc->enunciado}}</h3>
                                <p>{!! $doc->contenido !!}</p>
                                <a href="{{url('/'.$doc->archivo)}}" target="_blank"><button class="btn yellow">Descargar PDF</button></a>
                            </div>
                        </div>
                    @endforeach

                </div>
            </div>
        </div>



        <div class="row subsections my-5">
            <ol>
                <li >
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de bellezaaa</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>

    @php
        $i=1;
    @endphp
    @foreach($stra as $st)
        <div class="modal fade" id="estrategia{{$i}}" tabindex="-1" role="dialog" aria-labelledby="estrategia{{$i}}Label" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <div class="subtitle"><h1><b>{{$st->enunciado}}</b></h1></div>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body px-5">
                    {!! $st->contenido !!}   
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                </div>
                </div>
            </div>
        </div>
        @php
            $i++;
        @endphp
    @endforeach
    

@endsection
