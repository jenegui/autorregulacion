@extends('layout',['title' => 'Novedades','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'menu'=>$menu,'logos'=>$logos ])
@section('content')
    <div class="breadcrumb">
        <ul>
            <li><a href="{{url('/')}}">Inicio</a></li>
            <li><a href="{{url('/novedades')}}">Novedades</a></li>
        </ul>      
    </div>
    <div class="home_slider">
        <div id="subsideSlider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @php
                    $activo="active";
                @endphp
                @foreach($sliders as $slide)
                    <div class="carousel-item {{$activo}}">
                        <div class="container1">
                            <a href="{{url('/'.$slide->contenido)}}">
                                <img class="w-100" src="{{$slide->archivo}}" alt="">
                            </a>
                        </div>
                    </div>
                    @php
                        $activo="";
                    @endphp
                @endforeach
            </div>
            <ol class="carousel-indicators">
                @php
                    $activo="active";
                    $i=0;
                @endphp
                @foreach($sliders as $slide)
                    <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                        <span></span>                        
                    </li>
                    @php
                        $activo="";
                        $i++;
                    @endphp
                @endforeach
            </ol>
        </div>
    </div>

    <div class="row block noticias">
        <div class="col-12">
            <div class="row">
                <div class="col-12 col-lg-3 text-center">
                    <img class="w-80 m-auto my-4" src="img/novedades/noticias.svg" alt="alternative text">
                </div>
                <div class="col-12 col-lg-9">
                    <div class="subtitle">
                        <h1><b>Noticias</b></h1>
                    </div> 
                </div>
            </div>
        </div>
        <div class="articulos col-12 mt-4">
            <table id="newsTable" class="table w-100">
                <thead>
                    <tr><th> </th><th> </th></tr>
                </thead>
                @foreach($noticias as $n)
                    
                    <tr>
                        <td class="info">
                            <a href="{{url('/novedades/'.$n->id)}}">
                                <h4 class="text-blue">{!!$n->titulo!!}</h4>
                            </a>
                                <small>{{$n->created_at}}</small>
                                <div >{!!$n->resumen!!}</div>
                            <a href="{{url('/novedades/'.$n->id)}}">
                                <button class="btn yellow">Ver más</button>
                            </a>
                        </td>
                        <td class="text-center">
                            <img class="w-80 m-auto my-4" src="{{$n->imagen}}" alt="{{$n->titulo}}"> 
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>


    <div class="row block right">
        
        <div class="col-12 col-md-7 pl-4">
            <div class="subtitle">
                <h2>{!!$bloque1->enunciado!!}</h2>  
            </div>
            <div class="paragraph">
                {!!$bloque1->contenido!!}
            </div>
        </div>
        <div class="col-12 col-md-5 text-center">
            <img class="w-60 m-auto my-4" src="{{url('/'.$bloque1->archivo)}}" alt="Capacitaciones" title="Capacitaciones">
        </div>
    </div>


    <div class="row block right">
        <div class="col-12 col-md-5 text-center">
            <img class="w-60 m-auto my-4" src="{{url('/'.$bloque2->archivo)}}" alt="Legislaciones" title="Legislaciones">
        </div>
        <div class="col-12 col-md-7">
            <div class="subtitle">
                <h2>{!!$bloque2->enunciado!!}</h2>
            </div>
            <div class="paragraph">
                {!!$bloque2->contenido!!}
            </div>
        </div>
        
    </div>
    <div class="row block right">
        <div class="col-12 col-md-7">
            <div class="subtitle">
                <h2>{!!$bloque3->enunciado!!}</h2>
            </div>
            <div class="paragraph">
                {!!$bloque3->contenido!!}
            </div>
            <div class="text-left">
                <button class="btn yellow" data-toggle="modal" data-target="#consulta">Consulta aquí</button>
            </div>
        </div>
        <div class="col-12 col-md-5 text-center">
            <img class="w-80 m-auto my-4" src="{{url('/'.$bloque3->archivo)}}" alt="Concepto favorable" title="Concepto favorable">
        </div>
    </div>


    <div class="row subsections my-5">
            <ol>
                <li >
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>

    <!-- Modal -->
    <div class="modal fade" id="consulta" tabindex="-1" role="dialog" aria-labelledby="consultaLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <div class="subtitle"><b>Consulte ahora los negocios con concepto favorable</b></div>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body newsletter">
            <div class="my-4">
                <h3 class="text-blue">Filtros:</h3>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">Tipos de Establecimiento</label>
                    <select  name="" id="testablecimiento">
                        <option value="" selected>ninguno</option>
                        @foreach ($te as $t)
                            <option value="{{$t->id}}">{{$t->nombre_tipo_de_establecimiento}}</option>
                        @endforeach
                    </select>
                </div>
               <div class="form-group">
                    <span class="text-orange">• </span><label for="">Localidades</label>
                    <select  name="" id="localidad">
                        <option value="" selected>ninguno</option>
                        @foreach ($localidades as $loc)
                            <option value="{{$loc->codLocalidad}}">{{$loc->NombreLocalidad}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">UPZ</label>
                    <select  name="" id="upz">
                        <option value="" selected>ninguno</option>
                        @foreach ($upzs as $upz)
                           <option value="{{$upz->codUPZ}}">{{$upz->NombreUPZ}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <span class="text-orange">• </span><label for="">Barrio</label>
                    <select  name="" id="barrio">
                        <option value="" selected>ninguno</option>
                        @foreach ($barrios as $ba)
                            <option value="{{$ba->CodigoBarrio}}">{{$ba->NombreBarrio}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="right-content w-100 my-2">
                <input id="term" class="form-control mr-sm-2" type="text" placeholder="Nombre Comercial" aria-label="Nombre Comercial">
            </div>
            <button id="btn_consultar" class="btn yellow mb-4">Consultar</button>
            <button id="btn_nueva_consulta" class="btn yellow mb-4 ml-4">Nueva consulta</button>
            <table id="modalTable" class="table table-striped table-bordered w-100">
               <thead>
                    <tr>
                        <!--<th>Razón social</th>-->
                        <th>Nombre comercial</th>
                        <th>Dirección comercial</th>
                        <th>Fecha de visita</th>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
    </div>

@endsection


@section ("scripts")
    <script languague="javascript">           
        $(document).ready(function () {

            $("#btn_nueva_consulta").click(function(){
                $("#testablecimiento").val("");
                $("#localidad").val("");
                $("#barrio").val("");
                $("#upz").val("");
                $("#term").val("");
                $("#modalTable").DataTable().clear().draw(false);
            });


            $("#btn_consultar").click(function(){
                //$("#modalTable").html("");
                var theval=$("#term").val();

                var tf1=$("#testablecimiento").val();
                var tf2=$("#localidad").val();
                var tf3=$("#barrio").val();
                var tf4=$("#upz").val();

                $.post("{{url('/consultar')}}",{'term':theval.toString(),'tf1':tf1,'tf2':tf2,'tf3':tf3,'tf4':tf4,'_token':'{{ csrf_token() }}'},function(data){
                    console.log(data);
                    if(data=="error"){
                        //$("#modalTable").html("No hay resultados");
                        $("#modalTable").DataTable().clear().draw(false);
                    }else{
                        var jdata=JSON.parse(data);
                        $("#modalTable").DataTable().clear();
                        //var tabla="<thead><tr><th>Nombre Comercial</th><th>Razón Social</th></tr></thead>";
                        var tabla="";
                        jQuery.each(jdata,function(i,val){
                            tabla="<tr><td>"+val.NombreComercial+"</td><td>"+val.DireccionComercial+"</td><td>"+val.FechaVisita+"</td></tr>";
                            $("#modalTable").DataTable().row.add($(tabla));
                        });
                        
                        //$("#modalTable").html(tabla);
                    
                        $("#modalTable").DataTable().draw();
                    }
                });
            });
             $("#localidad").on('change',function(){
                var tf=$("#localidad").val();
                $.get("{{url('/filtroloc')}}",{'localidad':tf},function(data){
                    console.log(data);
                    if(data=="error"){
                        // no hago nada
                    }else{
                        $("#upz").html("");
                        var jdata=JSON.parse(data);
                        var html="<option value='' selected>ninguno</option>";
                        jQuery.each(jdata,function(i,val){
                            html+="<option value='"+val.codUPZ+"' selected>"+val.NombreUPZ+"</option>";
                        });
                        $("#upz").html(html);
                    }
                });
            });
            $("#upz").on('change',function(){
                var tf=$("#upz").val();
                $.get("{{url('/filtroupz')}}",{'upz':tf},function(data){
                    console.log(data);
                    if(data=="error"){
                        // no hago nada
                    }else{
                        $("#barrio").html("");
                        var jdata=JSON.parse(data);
                        var html="<option value='' selected>ninguno</option>";
                        jQuery.each(jdata,function(i,val){
                            html+="<option value='"+val.CodigoBarrio+"' selected>"+val.NombreBarrio+"</option>";
                        });
                        $("#barrio").html(html);
                    }
                });
            });

            $('#modalTable').DataTable( {
                searching: false,
                "pageLength": 5,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });


            $('#newsTable').DataTable( {
                searching: false,
                ordering:  false,
                "info": false,
                "lengthChange": false,
                "pageLength": 2,
                "language": {
                    "lengthMenu": "Mostrar _MENU_ Registros por página",
                    "zeroRecords": "No hay resultados.",
                    "info": "Mostrando página _PAGE_ de _PAGES_",
                    "infoEmpty": "No hay registros disponibles",
                    "infoFiltered": "(filtered from _MAX_ total records)",
                    "paginate": {
                        "first":      "Primero",
                        "previous":   "Anterior",
                        "next":       "Siguiente",
                        "last":       "Último"
                    },
                }
            });
            

        });
    </script>
@endsection