@extends('layout', ['title' => 'Registrate','faqs' => $faq,'te' => $te,'tips' => $tip,'access' => $access,'menu'=>$menu,'logos'=>$logos ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/registrate')}}">Regístrate</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @php
                            $activo="";
                        @endphp
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @php
                        $activo="active";
                        $i=0;
                    @endphp
                    @foreach($sliders as $slide)
                        <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                            <span></span>                        
                        </li>
                        @php
                            $activo="";
                            $i++;
                        @endphp
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2>{!!$bloque1->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque1->contenido!!}
                </div>
                <div>
                    <a href="https://drive.google.com/file/d/17LHFsBf1MHmjIP1aVMz3GcDW4q8W4UxD/view" target="blank"><button class="btn yellow">{!!$videotutorial->enunciado!!}</button></a>
                </div>    
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 m-auto my-4" src="{{url('/'.$bloque1->archivo)}}" alt="registrate" title="registrate">
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-4 text-center ">
                <img class="w-80 m-auto my-4" src="{{url('/'.$bloque2->archivo)}}" alt="establecimientos" title="establecimientos">
            </div>
            <div class="col-12 col-md-8 pl-4">
                <div class="subtitle">
                    <h2>{!!$bloque2->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque2->contenido!!}                    
                </div>
                <div class="registro_triada">
                    <a href="{{$triada[0]->contenido}}" target="_blank"><button class="btn yellow">{!!$triada[0]->enunciado!!}</button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href="{{$triada[1]->contenido }}"><button class="btn yellow">{!!$triada[1]->enunciado!!}
                        <div class="hover">
                            <div class="globo">{{$triada[3]->contenido}}</div>
                            <img src="{{url('/img/registro/Registrate-Alertas.svg')}}" alt="">
                        </div>
                    </button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href="{{$triada[2]->contenido}}" target="_blank"><button class="btn yellow">{!!$triada[2]->enunciado!!}
                        <div class="hover">
                            <div class="globo">{{$triada[4]->contenido}}</div>
                            <img src="{{url('/img/registro/Registrate-Alertas.svg')}}" alt="">
                        </div>
                    </button></a>
                </div>
            </div>
        </div>
        <div class="row block">
            <div class="col-12">
                <div class="mb-4">
                    <h2 class="text-blue"><b>Otros tipos</b> de inscripción </h2>
                </div>
                <div class="row right mb-4 pb-4">
                    <div class="col-12 col-md-7">
                        <div class="subtitle">
                            <h3>{!!$bloque3->enunciado!!}</h3>
                        </div>
                        <div class="paragraph">
                            {!!$bloque3->contenido!!}
                        </div>

                        <div class="text-left regitro_triada">
                            <a href="{{$triada[5]->contenido}}" target="_blank">
                                <button class="btn yellow">{!!$triada[5]->enunciado!!}</button>
                            </a>

                            <i class="fa fa-chevron-right"></i>

                            <a href="{{$triada[6]->contenido}}">
                            <button class="btn yellow">{!!$triada[6]->enunciado!!}
                            <div class="hover">
                                <div class="globo">{!!$triada[7]->contenido!!}</div>
                                <img src="{{url('/img/registro/Registrate-Alertas.svg')}}" alt="">
                            </div> 
                            </button></a>
                        </div>
                    </div>
                    <div class="col-12 col-md-5 text-center mt-4">
                        <img class="w-80 m-auto my-4" src="{{url('/'.$bloque3->archivo)}}" alt="vehiculos" title="vehiculos">
                    </div>
                </div>

                <div class="row mt-4 pt-4">
                    <div class="col-12 col-md-5 text-center">
                        <img class="m-auto" width="180" src="{{url('/'.$bloque4->archivo)}}" alt="tatuadores" title="tatuadores">
                    </div>
                    <div class="col-12 col-md-7">
                        <div class="subtitle">
                            <h3>{!!$bloque4->enunciado!!}</h3>
                        </div>
                        <div class="paragraph">
                            {!!$bloque4->contenido!!}
                        </div>
                        <div class="text-left">
                            <a href="http://appb.saludcapital.gov.co/MicroSivigilaDC/InscripcionTatuadores/frmSubmenuInscripcionTatus.aspx
" target="_blank"><button class="btn yellow">Regístrate</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="row" id="listasChequeo">
            <div class="col-12 col-md-5">
                <div class="subtitle">
                    <h1><b>Verifica</b></h1>
                </div>
                <div class="text-blue">
                    <h2><b>Confirma que cumples con la norma sanitaria</b></h2>
                </div>
                <div class="paragraph">
                    <p>
                        Para facilitar la autoevaluación del cumplimiento de los requisitos normativos, la Secretaría Distrital de Salud pone a disposición de los representantes legales y propietarios unos formatos de verificación —según la actividad comercial—, los cuales se deben diligenciar y cumplir en un 100 %, previo a la solicitud de la visita de inspección.
                    </p>
                </div>

                <div class="text-blue">
                    <h3><b>Instrucciones</b></h3>
                </div>
                <div class="">
                    <p><b><i>¿Cómo verifico y determino si mi establecimiento cumple con la norma sanitaria?</i></b></p>
                </div>
                <div class="numeracion">
                    <ol>
                        <li>Lee detenidamente la totalidad del formato de verificación</li>
                        <li>Toma cada criterio por evaluar y, de acuerdo con la realidad de tu establecimiento, registra si cumples o no.</li>
                        <li>En caso de no cumplir un criterio, procede a realizar las acciones correctivas que lleven al logro de este requisito.</li>
                        <li>Una vez cumplas con el 100% de los criterios, procede a solicitar una visita de inspección para obtener el respectivo concepto sanitario</li>
                        <li>Mantén siempre tu lista de chequeo a la mano y autoevalúate periódicamente, de tal forma que siempre te encuentres al día con estos requerimientos. </li>
                    </ol>
                </div>
            </div>

            <div class="col-12 col-md-7 block">
                <div class="verifica-lista">
                    <div class="verifica-lista-wrapper pb-4">
                        @foreach($listas as $l)
                            <div class="mb-4">
                                <h2 class="text-blue">{!!$l->enunciado!!}</h2>
                                <div class="paragraph">
                                    <div>
                                        {!!$l->contenido!!}
                                    </div>
                                    <div class="text-left">
                                        <a href="{{url('/'.$l->archivo)}}" target="_blank"><button class="btn yellow"><img src="{{url('/img/registro/verlista.png')}}"> Ver Lista</button></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>




        <div class="row block right">
            <div class="col-12 col-md-5">
                <img class="w-50 m-auto my-4" src="{{url('/'.$bloque5->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!! $bloque5->enunciado !!} </h2>
                </div>
                <div class="paragraph">
                    {!! $bloque5->contenido !!} 
                </div>
                <div class="text-right">
                    <a href="{!! $bloque6->contenido !!} " target="_blank"><button class="btn yellow">Solicita una visita</button></a>
                </div>
            </div>
        </div>





        <div class="row subsections my-5">
            <ol>
                <li >
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection
