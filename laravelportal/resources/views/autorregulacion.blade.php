@extends('layout', ['title' => 'Autorregulacion','faqs' => $faq,'te' => $te,'tips' => $tip,'access'=>$access,'menu'=>$menu,'logos'=>$logos ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/autorregulacion')}}">Autorregulacion</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @php
                            $activo="";
                        @endphp
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @php
                        $activo="active";
                        $i=0;
                    @endphp
                    @foreach($sliders as $slide)
                        <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                            <span></span>                        
                        </li>
                        @php
                            $activo="";
                            $i++;
                        @endphp
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row block right" id="quees">
            <div class="col-12 col-md-5 text-center">
                <img class="w-60 m-auto my-4" src="{{url('/'.$bloque1->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!!$bloque1->enunciado!!}</h2>    
                </div>
                <div class="paragraph">
                    {!!$bloque1->contenido!!}       
                </div>
            </div>
        </div>

        <div class="row subsections pasos my-5" id="proceso">
            <ol class="w-100">
                <li class="active text-center">
                    <a href="#paso1">
                    <div class="text-big">{!!$pasosbotones[0]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso1Blanco.svg" alt="paso 1" title="paso 1">
                    <div>{!!$pasosbotones[0]->contenido!!}</div>
                    </a>
                </li>
                <li class="active text-center">
                    <a href="#paso2">
                    <div class="text-big">{!!$pasosbotones[1]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso2Blanco.svg" alt="paso 2" title="paso 2">
                    <div>{!!$pasosbotones[1]->contenido!!}</div>
                    </a>
                </li>
                <li class="active text-center">
                    <a href="#paso3">
                    <div class="text-big">{!!$pasosbotones[2]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso3Blanco.svg" alt="paso 3" title="paso 3">
                    <div>{!!$pasosbotones[2]->contenido!!}</div>
                    </a>
                </li>
                <li class="active text-center">
                    <a href="#paso4">
                    <div class="text-big">{!!$pasosbotones[3]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso4Blanco.svg" alt="paso 4" title="paso 4">
                    <div>{!!$pasosbotones[3]->contenido!!}</div>
                    </a>
                </li>
                <li class="active text-center">
                    <a href="#paso5">
                    <div class="text-big">{!!$pasosbotones[4]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso5Blanco.svg" alt="paso 5" title="paso 5">
                    <div>{!!$pasosbotones[4]->contenido!!}</div>
                    </a>
                </li>
            </ol>
        </div>

        <div id="paso1" class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue"><b>Paso 1</b></h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso1Azul.svg" alt="paso 1" title="paso 1">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>{!!$pasosbloques[0]->enunciado!!}</h2>
                </div>
                
                <p>{!!$pasosbloques[0]->contenido!!}</p>
                <div class="btn yellow"><a class="text-white" href="{{url('/registrate')}}" >Conócela</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100 w-sm-50" src="img/autorregula/a1.png" alt="">
            </div>
        </div>
        <div id="paso2" class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue"><b>Paso 2</b></h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso2Azul.svg" alt="paso 2" title="paso 2">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>{!!$pasosbloques[1]->enunciado!!}</h2>
                </div>
                <p>{!!$pasosbloques[1]->contenido!!}</p>
				<!--Ajuste URL Produccion Autor Mario Beltran Fecha 09052019 -->
                <div class="btn yellow"><a class="text-white" href="http://appb.saludcapital.gov.co/MicroSivigilaDC/ServiciosComuni1.aspx" target="_blank">Regístrate</a></div>
                
                
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100 w-sm-50" src="img/autorregula/a2.png" alt="">
            </div>
        </div>
        <div id="paso3" class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue"><b>Paso 3</b></h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso3Azul.svg" alt="paso 3" title="paso 3">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>{!!$pasosbloques[2]->enunciado!!}         </h2>
                </div>
                <p>{!!$pasosbloques[2]->contenido!!}</p>
                <div class="btn yellow"><a class="text-white" href="{{url('/registrate#listasChequeo')}}" >Verifica</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100 w-sm-50" src="img/autorregula/a3.png" alt="">
            </div>
        </div>
        <div id="paso4" class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue"><b>Paso 4</b></h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso4Azul.svg" alt="paso 4" title="paso 4">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>{!!$pasosbloques[3]->enunciado!!}</h2>
                </div>
                <p>{!!$pasosbloques[3]->contenido!!}</p>
                <div class="btn yellow"><a class="text-white" href="http://conceptosanitario.saludcapital.gov.co/" target="_blank" >Solicita visita</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100 w-sm-50" src="img/autorregula/a4.png" alt="">
            </div>
        </div>
        <div id="paso5" class="row block">
            <div class="col-12 col-lg-3 text-center">
                <h2 class="text-blue"><b>Paso 5</b></h2>
                <img class="bluehand" src="img/icons/AutorregulaPaso5Azul.svg" alt="paso 5" title="paso 5">
            </div>
            <div class="col-12 col-lg-7">
                <div class="subtitle">
                    <h2>{!!$pasosbloques[4]->enunciado!!}</h2>
                </div>
                <p>{!!$pasosbloques[4]->contenido!!}</p>
                <div class="btn yellow"><a class="text-white" href="{{url('/buenpropietario')}}" >Autorregúlate</a></div>
            </div>
            <div class="col-12 col-lg-2">
                <img class="w-100 w-sm-50" src="img/autorregula/a5.png" alt="">
            </div>
        </div> 




        <div class="row block" id="concepto">
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 m-auto my-4" src="{{url('/'.$bloque2->archivo)}}" alt="concepto sanitario">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!!$bloque2->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque2->contenido!!}
                </div>
            </div>
        </div>


        <div class="row block mapa" id="quien">
            <div class="col-12">
                <div class="subtitle">
                    <h2>{!! $bloque5->enunciado !!}</h2>
                </div>
            </div>
            <div class="col-12 col-md-7">
                <div id="mapa">
                    <img class="w-100" src="{{ url('/').'/'.$bloque5->archivo }}" alt="">
                </div>
                <div class="text-white mt-4">
                    {!! $bloque5->contenido !!}
                </div>
            </div>
            <div class="col-12 col-md-5 text-white">
                <ul class="mapaExplain">
                    @foreach($redes as $red)
                        <li>
                            <i><img src="{{ url('/').'/'.$red->archivo }}" alt=""></i>
                            <div><b>{!! $red->enunciado !!}</b> {!! $red->contenido !!}</div> 
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>


        
        <div class="row block">
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2> {!!$bloque3->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque3->contenido!!}
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-70 m-auto my-4" src="{{url('/'.$bloque3->archivo)}}" alt="alternative text">
            </div>
        </div>
        <div class="row block right">
            <div class="col-12 col-md-7">
                <div class="subtitle">
                    <h2> {!!$bloque4->enunciado!!}</h2>
                </div>
                <div class="paragraph">
                    {!!$bloque4->contenido!!}
                </div>
            </div>
            <div class="col-12 col-md-5 text-center">
                <img class="w-60 m-auto my-4" src="{{url('/'.$bloque4->archivo)}}" alt="alternative text">
            </div>
        </div>





        <div class="row subsections my-5">
            <ol>
                <li>
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>
@endsection
