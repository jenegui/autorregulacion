@extends('layout', ['title' => 'Droguerías','faqs' => $faq,'te' => $te,'tips' => $tip,'menu'=>$menu,'logos'=>$logos ])
@section('content')
        <div class="breadcrumb">
            <ul>
                <li><a href="{{url('/')}}">Inicio</a></li>
                <li><a href="{{url('/droguerías')}}">Droguerías</a></li>
            </ul>      
        </div>
        <div class="home_slider">
            <div id="subsideSlider" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                    @php
                        $activo="active";
                    @endphp
                    @foreach($sliders as $slide)
                        <div class="carousel-item {{$activo}}">
                            <div class="container1">
                                <a href="{{url('/'.$slide->contenido)}}">
                                    <img class="w-100" src="{{$slide->archivo}}" alt="">
                                </a>
                            </div>
                        </div>
                        @php
                            $activo="";
                        @endphp
                    @endforeach
                </div>
                <ol class="carousel-indicators">
                    @php
                        $activo="active";
                        $i=0;
                    @endphp
                    @foreach($sliders as $slide)
                        <li data-target="#subsideSlider" data-slide-to="{{$i}}" class="{{$activo}}">
                            <span></span>                        
                        </li>
                        @php
                            $activo="";
                            $i++;
                        @endphp
                    @endforeach
                </ol>
            </div>
        </div>

        <div class="row block right">
            <div class="col-12 col-md-5 text-center">
                <img class="w-50 m-auto my-4" src="{{url('/'.$bloque1->archivo)}}" alt="alternative text">
            </div>
            <div class="col-12 col-md-7 pl-4">
                <div class="subtitle">
                    <h2>{!! $bloque1->enunciado !!}</h2>
                </div>
                <div class="paragraph">
                    {!! $bloque1->contenido !!}
                </div>
            </div>
        </div>





        <div class="row block">
            <div class="col-12 col-md-10">
                <div class="subtitle">
                    <h2><b>¿Qué requerimientos</b> se deben cumplir?</h2>
                </div>
            </div>
            <div class="col-12">
                <div class="row requerimientos">
                    @foreach($requerimientos as $requerimiento)
                    <div class="col-12 col-md-6">
                        <div class="row">
                            <div class="col-3">
                                <img src="{{ $requerimiento->archivo }}" alt="" class="w-100 mb-4">
                            </div>
                            <div class="col-9 align-middle">
                                <div>
                                    {!! $requerimiento->enunciado !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                {!! $requerimiento->contenido !!}
                            </div>
                            
                        </div>
                    </div>
                    @endforeach

                    
                </div>
            </div>
        </div>





        <div class="row mt-5">
            <div class="col-12 col-md-12">
                <div class="subtitle">
                    <h2><b>Proceso de autorregulación</b> para Droguerías</h2>
                </div>
            </div>
        </div>
        <div class="row subsections pasos my-5">
            <ol class="w-100">
                <li class="active text-center">
                    <div class="text-big">{!!$pasosbotones[0]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso1Blanco.svg" alt="paso 1" title="paso 1">
                    <div class="texto">{!!$pasosbotones[0]->contenido!!}</div>
                    <div><button class="btn yellow" data-toggle="modal" data-target="#paso1" >Ver más</button></div>
                </li>
                <li class="active text-center">
                    <div class="text-big">{!!$pasosbotones[1]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso2Blanco.svg" alt="paso 2" title="paso 2">
                    <div class="texto">{!!$pasosbotones[1]->contenido!!}</div>
                    <div><button class="btn yellow" data-toggle="modal" data-target="#paso2" >Ver más</button></div>
                </li>
                <li class="active text-center">
                    <div class="text-big">{!!$pasosbotones[2]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso3Blanco.svg" alt="paso 3" title="paso 3">
                    <div class="texto">{!!$pasosbotones[2]->contenido!!}</div>
                    <div><button class="btn yellow" data-toggle="modal" data-target="#paso3" >Ver más</button></div>
                </li>
                <li class="active text-center">
                    <div class="text-big">{!!$pasosbotones[3]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso4Blanco.svg" alt="paso 4" title="paso 4">
                    <div class="texto">{!!$pasosbotones[3]->contenido!!}</div>
                    <div><button class="btn yellow" data-toggle="modal" data-target="#paso4" >Ver más</button></div>
                </li>
                <li class="active text-center">
                    <div class="text-big">{!!$pasosbotones[4]->enunciado!!}</div>
                    <div class="line"></div>
                    <img src="img/icons/AutorregulaPaso5Blanco.svg" alt="paso 5" title="paso 5">
                    <div class="texto">{!!$pasosbotones[4]->contenido!!}</div>
                    <div><button class="btn yellow" data-toggle="modal" data-target="#paso5" >Ver más</button></div>
                </li>
            </ol>
        </div>

        <div class="text-center">
            <div class="registro_triada">
                    <a href="{{$triada[14]->contenido}}" target="_blank"><button class="btn yellow">{!!$triada[0]->enunciado!!}</button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href="{{$triada[15]->contenido}}" target="_blank"><button class="btn yellow">{!!$triada[1]->enunciado!!}
                        <div class="hover">
                            <div class="globo">{{$triada[3]->contenido}}</div>
                            <img src="{{url('/img/registro/Registrate-Alertas.svg')}}" alt="">
                        </div>
                    </button></a>
                    <i class="fa fa-chevron-right"></i>
                    <a href="{{$triada[16]->contenido}}" target="_blank"><button class="btn yellow">{!!$triada[2]->enunciado!!}
                        <div class="hover">
                            <div class="globo">{{$triada[4]->contenido}}</div>
                            <img src="{{url('/img/registro/Registrate-Alertas.svg')}}" alt="">
                        </div>
                    </button></a>
                </div>
        </div>

        


        <div class="row block my-5">
            <div class="col-12 col-md-12">
                <div class="subtitle">
                    <h2><b>Normativa sanitaria</b></h2>
                </div>
                <div class="normativas">
                    @foreach($docs as $doc)
                        <div class="normativa">
                            <div class="icon"><img src="img/buenpro/ley.svg" alt="recomendacion" title="recomendacion"></div>
                            <div class="content">
                                <h3 class="text-blue">{{$doc->enunciado}}</h3>
                                <p>{!! $doc->contenido !!}</p>
                                <a href="{{url('/'.$doc->archivo)}}" target="_blank"><button class="btn yellow">Descargar PDF</button></a>
                            </div>
                        </div>
                    @endforeach
                   
                </div>
            </div>
        </div>



        <div class="row subsections my-5">
            <ol>
                <li>
                    <div class="logo logo_rest"></div>
                    <div class="text">Restaurantes</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/restaurantes')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_carne"></div>
                    <div class="text">Expendios de Carne</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/carnicerias')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li class="active">
                    <div class="logo logo_drog"></div>
                    <div class="text">Droguerías</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/droguerías')}}" >Ver requisitos</a></div>
                    </div>
                </li>
                <li>
                    <div class="logo logo_salon"></div>
                    <div class="text">Salones de belleza</div>
                    <div class="text-right mt-2">
                        <div class="btn yellow"><a href="{{url('/salonesbelleza')}}" >Ver requisitos</a></div>
                    </div>
                </li>
            </ol>
        </div>





    <div class="modal fade" id="paso1" tabindex="-1" role="dialog" aria-labelledby="paso1Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <div class="subtitle"><h2><b>{!!$pasosmodales[0]->enunciado!!}</b></h2></div>
                <p>
                   {!!$pasosmodales[0]->contenido!!}
                </p>
                <div>
                    <a href="{{url('/registrate')}}">
                    <button class="btn yellow">Ver más</button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paso2" tabindex="-1" role="dialog" aria-labelledby="paso2Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <div class="subtitle"><h2><b>{!!$pasosmodales[1]->enunciado!!}</b></h2></div>
                <p>
                    {!!$pasosmodales[1]->contenido!!}
                </p>
                <div>
                    <a href="http://appb.saludcapital.gov.co/MicroSivigilaDC/ServiciosComuni1.aspx" target="_blank">
                    <button class="btn yellow">Ver más</button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paso3" tabindex="-1" role="dialog" aria-labelledby="paso3Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <div class="subtitle"><h2><b>{!!$pasosmodales[2]->enunciado!!}</b></h2></div>
                <p>
                    {!!$pasosmodales[2]->contenido!!}
                </p>
                <div>
                    <a href="{{url('/listas/Autoevaluacion_de_droguerias.pdf')}}" target="_blank">
                    <button class="btn yellow">Ver más</button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paso4" tabindex="-1" role="dialog" aria-labelledby="paso4Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <div class="subtitle"><h2><b>{!!$pasosmodales[3]->enunciado!!}</b></h2></div>
                <p>
                    {!!$pasosmodales[3]->contenido!!} 
                </p>
                <div>
                    <a href="http://conceptosanitario.saludcapital.gov.co/pages/index.php" target="_blank">
                        <button class="btn yellow">Ver más</button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="paso5" tabindex="-1" role="dialog" aria-labelledby="paso5Label" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body px-5">
                <div class="subtitle"><h2><b>{!!$pasosmodales[4]->enunciado!!}</b></h2></div>
                <p>
                    {!!$pasosmodales[4]->contenido!!}
                </p>
                <div>
                    <a href="{{url('/buenpropietario')}}">
                        <button class="btn yellow">Ver más</button>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
            </div>
        </div>
    </div>



@endsection