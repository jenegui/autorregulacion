@extends('admin.layout',['logged'=>$logged,'tipos'=>$tipos])

@section('sectionTitle') Noticia @endsection
@section('content')
    <script src="{{url('/')}}/js/tinymce/tinymce.min.js"></script>
	<script type="text/javascript">
		var editor_config = {
        	path_absolute:"{{ URL::to('/') }}/",
        	selector:"textarea",
        	relative_urls: false
        	
        };
        tinymce.init(editor_config);
	</script>

    <form action="{{url('/admin/saveNoticia')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <input name="idn" type="hidden" value="{{$result->id}}">
        <div class="form-group mt-2">
            <label for="">Título</label>
            <input name="titulo" type="text" class="form-control" value="{{$result->titulo}}">
        </div>
        <div class="form-group mt-2">
            <label for="">Resumen</label>
            <textarea name="resumen" class="form-control" rows="3">{{$result->resumen}}</textarea>
        </div>
        <div class="form-group mt-2">
            <label for="">Contenido</label>
            <textarea name="contenido" class="form-control" rows="3">{{$result->contenido}}</textarea>
        </div>
        <div class="form-group mt-2">
            <label for="">Pie de foto</label>
            <textarea name="piefoto" class="form-control" rows="3">{{$result->piefoto}}</textarea>
        </div>
        <div class="row">
            <div class="col-12 col-md-6">
                <div class="form-group mt-2">
                    <label for="">Imágen</label>
                    <input name="imagen" type="file">
                </div>
            </div>
            <div class="col-12 col-md-6">
                @if( !empty($result->imagen) )
                    <img class="w-100" src="{{url('/'.$result->imagen) }}" alt="">
                @endif
            </div>
        </div>
        <div class="row">
            <div class="col-3">
                <button type="submit">Guardar</button>
            </div>
            <div class="col-3">
                @if(!empty($result->id))
                    <a href="{{url('admin/tooglenoticia/'.$result->id)}}">
                    @if($result->estado==0)
                        <button type="button">Activar</button>
                    @else
                        <button type="button">Desactivar</button>
                    @endif
                    </a>
                @endif
            </div>
        </div>
    </form>

@endsection


@section('scripts')
    
@endsection