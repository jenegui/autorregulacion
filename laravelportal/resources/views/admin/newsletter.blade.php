@extends('admin.layout',['logged'=>$logged,'tipos'=>$tipos])

@section('sectionTitle') Newsletter @endsection
@section('content')

    
    <form action="{{url()->current()}}" method="POST" >
        @csrf
        <div class="row">
            <div class="col-12 col-sm-6 col-lg-2">Buscar por</div>
            <div class="col-12 col-sm-6 col-lg-2">
                <select class="form-control" name="filter" id="filter">
                    <option value="1">Nombre</option>
                    <option value="2">Correo Electrónico</option>
                </select>
            </div>
        </div>
        <div class="row mt-2">
            <div class="col-12 col-sm-6 col-lg-2">
                <input name="term" type="text" class="form-control" placeholder="Término de búsqueda">
            </div>
            <div class="col-12 col-sm-6 col-lg-2">
                <button type="submit" class="btn btn-primary">Buscar</button>
            </div>
        </div>
    </form>
    <table class="table mt-4">
        <tr>
            <th>Nombre</th>
            <th>Correo Electrónico</th>
            <th>Teléfono</th>
            <th>Tipo de Establecimiento</th>
        </tr>
        @forelse( $results as $result)
        <tr>
            <td>{{$result->name}}</td>
            <td>{{$result->correo_e}}</td>
            <td>{{$result->telefono}}</td>
            <td>{{$result->establecimiento->nombre_tipo_de_establecimiento}}</td>
        </tr>
        @empty
        <tr>
            <td>-</td>
            <td>-</td>
            <td>-</td>
            <td>-</td>
        </tr>
        @endforelse    
    </table>
    @if(!empty($results))
    <div class="row mt-4">
        <div class="col-12">
            {{ $results->links()}}
        </div>
    </div>
    <div class="row mt-4">
        <div class="col-12">
            <!--<a href="{{ url('/admin/descarga') }}"><button>Descargar</button></a>      /export-->
            <form method="post" action="{{url('/export')}}"> 
               {{ csrf_field() }}
               <input type="submit" name="exportexcel" value='Excel Export'>
               <input type="submit" name="exportcsv" value='CSV Export'>
             </form>

        </div>
    </div>

    @endif

@endsection