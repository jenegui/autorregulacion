<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Error 404</title>

    <style>
        main{
            padding-left:2em;
            font-family:sans-serif;
        }
        h1{
            margin-top:2em;
            margin-bottom:3em;
        }
        h4{
            background:#005ca9;
            color:white;
            font-weight:bold;
            padding: 3em 1em;
        }
    </style>
</head>
<body>
    <main>    
    <h1>ERROR 23000</h1>
    <h4>Página no encontrada.</h4>
    </main>
</body>
</html>