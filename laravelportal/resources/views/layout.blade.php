<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Negocios Saludables, Negocios Rentables</title>
    <link rel="apple-touch-icon" sizes="57x57" href="{{url('/favicon/apple-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{url('/favicon/apple-icon-60x60.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{url('/favicon/apple-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('/favicon/apple-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{url('/favicon/apple-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120" href="{{url('/favicon/apple-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144" href="{{url('/favicon/apple-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152" href="{{url('/favicon/apple-icon-152x152.png')}}">
    <link rel="apple-touch-icon" sizes="180x180" href="{{url('/favicon/apple-icon-180x180.png')}}">
    <link rel="icon" type="image/png" sizes="192x192"  href="{{url('/favicon/android-icon-192x192.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{url('/favicon/favicon-32x32.png')}}">
    <link rel="icon" type="image/png" sizes="96x96" href="{{url('/favicon/favicon-96x96.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/favicon/favicon-16x16.png')}}">
    <link rel="manifest" href="{{url('/favicon/manifest.json')}}">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="{{url('favicon/ms-icon-144x144.png')}}">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=M+PLUS+Rounded+1c:400,800" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css"
        integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <link rel="stylesheet" href="{{asset('css/styles.css')}}">
    
    <link rel="stylesheet" href="{{asset('css/select2.css')}}">
    @isset($access)
    <link rel="stylesheet" href="{{asset('css/accesibilidad.css')}}">
    @endisset

    <!-- GOOGLE ANALITICS -->

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-146125889-1"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-146125889-1');
    </script>

    <!-- FIN GOOGLE ANALITICS -->


    <!-- Facebook Pixel Code -->

    <script>

        !function(f,b,e,v,n,t,s)

        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?

        n.callMethod.apply(n,arguments):n.queue.push(arguments)};

        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';

        n.queue=[];t=b.createElement(e);t.async=!0;

        t.src=v;s=b.getElementsByTagName(e)[0];

        s.parentNode.insertBefore(t,s)}(window,document,'script',

        'https://connect.facebook.net/en_US/fbevents.js');


        fbq('init', '498238717246798'); 

        fbq('track', 'PageView');

    </script>

    <noscript>

        <img height="1" width="1" 

        src="https://www.facebook.com/tr?id=498238717246798&ev=PageView

        &noscript=1"/>

    </noscript>

<!-- End Facebook Pixel Code -->

</head>
<body>
    
    <header>
        <div class="container">
            <div class="logos">
                <div class="row">
                    <div class="col-12 col-md-6">
                        <img class="logo1" src="{{url('/'.$logos[0]->archivo)}}" alt="" title="{{$logos[0]->enunciado}}">
                    </div>
                    <div class="col-12 col-md-6 text-lg-right">
                        <img class="logo2" src="{{url('/'.$logos[1]->archivo)}}" alt="" title="{{$logos[1]->enunciado}}">
                    </div>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-dark mt-2">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                    aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarCollapse">
                    <div class="mr-2">
                        <div class="locator mb-2 {{$title}}">
                            <div class="point"><span></span></div>
                        </div>
                        <ul class="navbar-nav">
                            <li class="nav-item active">
                                <a class="nav-link" href="{{url('/')}}">{{$menu[0]->enunciado}}<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                        <a class="nav-link" href="{{url('/autorregulacion')}}">{{$menu[1]->enunciado}}</a>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{url('/autorregulacion#quees')}}">{{$menu[2]->enunciado}}</a>
                                        <a class="dropdown-item" href="{{url('/autorregulacion#proceso')}}">{{$menu[3]->enunciado}}</a>
                                        <a class="dropdown-item" href="{{url('/autorregulacion#concepto')}}">{{$menu[4]->enunciado}}</a>
                                        <a class="dropdown-item" href="{{url('/autorregulacion#quien')}}">{{$menu[5]->enunciado}}</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/registrate')}}">{{$menu[6]->enunciado}}</a>
                            </li>
                            <li class="nav-item">
                                <div class="dropdown">
                                        <a class="nav-link" href="{{url('/buenpropietario')}}">{{$menu[7]->enunciado}}</a>
                                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{url('/buenpropietario#estrategiasexcelencia')}}">{{$menu[8]->enunciado}}</a>
                                    </div>
                                </div>
                                
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/novedades')}}">{{$menu[9]->enunciado}}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('/informes')}}">{{$menu[10]->enunciado}}</a>
                            </li>
                        </ul>
                    </div>
                    <div class="right-content">
                            <form action="{{url('/busqueda')}}" method="post">
                                @csrf
                                <input name="term" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Buscar">
                                <button class="buscar"></button>
                            </form>
                        <div class="dropdown text-center">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown"
                                aria-haspopup="true" aria-expanded="false">
                                Accesibilidad
                            </button>
                            <div class="dropdown-menu " aria-labelledby="dropdownMenuButton">

                                <a class="dropdown-item" href="{{url('/accesibilidad')}}">
                                @php
                                    if( isset($access ) ){
                                        echo "Original";
                                    }else{
                                        echo "Alto contraste";
                                    }
                                @endphp
                                
                                </a>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </nav>
        </div>
        
    </header>
    <main class="container">
        
        @yield('content')
        <div class="row mt-4">
            <div class="col-12 col-md-6 mt-4">
                <div class="subtitle">
                   <h2><b>Preguntas Frecuentes</b></h2> 
                </div>
                <div class="faqs-btn form-buscar mt-2 mt-md-0">
                    <input id="autocomplete" class="form-control mr-sm-2 buscar" type="text" placeholder="Buscar" aria-label="Buscar">
                    <button class="buscar"></button>
                </div>
                <div class="faqs">
                    <div class="faq_wrapper">
                        <div id="faq_accordion">
                            @php 
                            $i=0;
                            @endphp 
                            @foreach($faqs as $f)
                                @php 
                                $i++;
                                @endphp
                                <div class="card">
                                    <div class="card-header" id="heading{{$i}}">
                                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse{{$i}}" aria-expanded="false"
                                                aria-controls="collapse{{$i}}">
                                                {{$f->enunciado}}
                                            </button>
                                            
                                    </div>
                            
                                    <div id="collapse{{$i}}" class="collapse" aria-labelledby="heading{{$i}}" data-parent="#faq_accordion">
                                        <div class="card-body">
                                            {!!$f->contenido!!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 mt-4">
                <div class="subtitle">
                    <h2><b>Tips de autorregulación</b></h2> 
                </div>
                <ul class="tips">
                    @foreach($tips as $tip)
                    <li>
                        <img src="{{url('/').'/'.$tip->archivo}}" alt="">
                        {!!$tip->contenido!!}
                    </li>
                    @endforeach
                    
                </ul>
            </div>


        </div>
    </main>
    <div class="gray-block">
        <main class="container">
            <div class="row newsletter">
                <div class="col-12 col-md-6 text-md-center px-4">
                    <img class="img_newsletter" src="img/icons/home_newsletter.svg" alt="">
                    <div class="text-left">
                        <h3 class="text-orange">Envíanos tus datos</h3>
                        <h3 class="text-blue">y recibe información de tu negocio</h3>
                        <p class="mt-4">Suscríbete en este espacio y recibe información de la Secretaría Distrital de Salud con los requerimientos sanitarios que rigen a tu establecimiento, así como la actualización de la norma.</p>
                    </div>
                </div>
                <div class="col-12 col-md-6">
                    <form action="{{url('/registro')}}" method="post">
                        @csrf
                        <div class="form-newsletter">
                            <div class="input-group">
                                <!--<div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-bars"></i></span>
                                </div>-->
                                <select class="select2" name="tipo" id="tipo">
                                    <option value ="-">Seleccione...</option>
                                    @foreach ($te as $t)
                                        <option value="{{$t->id}}">{{$t->nombre_tipo_de_establecimiento}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-user"></i></span>
                                </div>
                                <input id="name" name="nombre" type="text" class="form-control" placeholder="Nombre" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-envelope"></i></span>
                                </div>
                                <input id="correo" name="correo" type="text" class="form-control" placeholder="Correo electrónico" required>
                            </div>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="inputIconBox"><i class="fas fa-phone"></i></span>
                                </div>
                                <input id="telefono" name="telefono" type="number" class="form-control telefono" placeholder="Teléfono" pattern="^228\d{8}$" required>
                            </div>
                            <div class="captcha">

                            </div>
                            <div class="input-group form-check conditions">
                                <input name="checkbox" type="checkbox" class="" id="check1" required>
                                <p>
                                    Acepta los términos y condiciones: autorizo expresamente a la Secretaría Distrital de Salud y el Fondo Financiero
                                    Distrital de Salud, para hacer uso y tratamiento de mis datos personales de conformidad con lo previsto en el
                                    Decreto 1377 de 2013 que reglamenta la Ley 1581 de 2012. (Política de Protección de Datos Personales)
                                </p>
                            </div>
                            <button id="btn_submit" class="btn yellow w-100 py-2" type="submit">Suscribirme</button>
                        </div>
                    </form>
                </div>
            </div>
        </main>
    </div>
    @yield("redes")
    





    <div class="container enlaces">
        <div class="row">
            <div class="col-12 col-md-3">
                <div class="title">Entes de Control</div>
                <ul>
                    <li><a target="_blank" href="http://www.personeriabogota.gov.co/">Personería de Bogotá</a></li>
                    <li><a target="_blank" href="https://www.procuraduria.gov.co/portal/">Procuraduría General de la Nación</a></li>
                    <li><a target="_blank" href="https://www.contraloria.gov.co/">Contraloría General de la Nación</a></li>
                    <li><a target="_blank" href="http://concejodebogota.gov.co/cbogota/site/edic/base/port/inicio.php">Concejo de Bogotá</a></li>
                    <li><a target="_blank" href="http://www.veeduriadistrital.gov.co/">Veeduría Distrital</a></li>
                    <li><a target="_blank" href="https://www.contratacionbogota.gov.co/es/web/cav3/ciudadano">Portal de Contratación a la Vista</a></li>
                    <li><a target="_blank" href="https://www.contratos.gov.co/puc/buscador.html">Portal de Contratación - SECOP</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Entes del Gobierno</div>
                <ul>
                    <li><a target="_blank" href="https://www.minsalud.gov.co/Paginas/default.aspx">Ministerio de Salud y Protección Social</a></li>
                    <li><a target="_blank" href="http://estrategia.gobiernoenlinea.gov.co/623/w3-channel.html">Gobierno Digital</a></li>
                    <li><a target="_blank" href="https://www.gov.co/">No más filas</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Menú principal</div>
                <ul>
                    <li><a target="_blank" href="http://www.saludcapital.gov.co/Paginas2/Inicio.aspx">Inicio</a></li>
                    <li><a target="_blank" href="http://www.saludcapital.gov.co/Paginas2/MisionyVision.aspx">La Entidad</a></li>
                    <li><a target="_blank" href="http://saludambiental.saludcapital.gov.co/">Salud Ambiental</a></li>
                    <li><a target="_blank" href="http://www.saludcapital.gov.co/Paginas2/Su-Bogota-vital.aspx">Bogotá Vital es Salud Urbana</a></li>
                    <li><a target="_blank" href="http://www.saludcapital.gov.co/Paginas2/Tramitesyservicios.aspx">Agilínea</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-3">
                <div class="title">Secretaría Distrital de Salud</div>
                <ul>
                    <li><a target="_blank" href="https://www.google.com/maps/place/Secretar%C3%ADa+Distrital+de+Salud/@4.6161635,-74.0947824,17.83z/data=!4m5!3m4!1s0x8e3f996f336f8a7b:0x183024e16c3df506!8m2!3d4.6155823!4d-74.0941572">Cra 32# 12-81 Bogotá, Colombia</a></li>
                    <li><a href="tel:57-1-3649090">Teléfono: (571) 3649090</a></li>
                    <li>Código Postal: 0571</li>
                    <li><a href="mailto:contactenos@saludcapital.gov.co">contactenos@saludcapital.gov.co</a></li>
                </ul>
            </div>
        </div>
    </div>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4"></div>
                <div class="col-12 col-md-4 text-md-center">
                    <p>2019. @ Todos los derechos reservados</p>
                    <p><a target="_blank" href="http://www.saludcapital.gov.co/Documents/Politica_Proteccion_Datos_P.pdf">*Habeas data</a></p>
                    <p><a href="http://www.saludcapital.gov.co/Documents/Politicas_Sitios_Web.pdf" target="_blank">*Términos y condiciones</a></p>
                </div>
                <div class="col-12 col-md-4 text-md-right">
                    <img class="logo3" src="{{url('/'.$logos[2]->archivo)}}" alt="" title="{{$logos[2]->enunciado}}">
                </div>
            </div>
        </div>
    </footer>


    <script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="{{asset('js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('js/jquery.autocomplete.min.js')}}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{asset('js/code.js')}}"></script>
    <script src="{{asset('js/select2.js')}}"></script>
    <!--script src="{{asset('js/select2_locale_es.js')}}"></script-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

    <script>
        $(document).ready(function () {
            var preguntasf = [
                @php 
                    $i=0;
                @endphp 
                @foreach($faqs as $f)
                    @php 
                        $i++;
                    @endphp
                    { value: '{{$f->enunciado}}', data: '{{$i}}' },
                
                @endforeach
                ];

                // setup autocomplete function pulling from preguntasf[] array
                $('#autocomplete').autocomplete({
                    lookup: preguntasf,
                    onSelect: function (suggestion) {
                        var ids=suggestion.data-1;
                        $(".faqs .card").hide();

                        $(".faqs .card").eq(ids).show();
                        
                    }
                });


                $('.select2').select2({
                    
                    width: '100%',
                    
                   
                });


                /*$('.select').parent().find('.select2-container').css('border', '1px solid #eae7e7');
                $('.select').parent().find('.select2-container').css('height', '150px');
                
                
                $('.select').parent().find('.select2-container').css('border-radius','35px 0px 35px 0px');
                $('.select').parent().find('.select2-container').css('-moz-border-radius','35px 0px 35px 0px');
                $('.select').parent().find('.select2-container').css('-webkit-border-radius','35px 0px 35px 0px');*/


        });
    </script>
    @yield("scripts")
    
</body>
</html>