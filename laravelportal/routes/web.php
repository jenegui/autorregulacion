<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@home');
Route::post('/registro','MainController@register');
Route::get('/autorregulacion', 'MainController@autorregulacion');
Route::get('/registrate', 'MainController@registrate');
Route::get('/buenpropietario','MainController@buenpropietario' );
Route::get('/novedades/{id}','MainController@noticia' );
Route::get('/novedades','MainController@novedades' );
Route::get('/informes', 'MainController@informe');



Route::post('/busqueda', 'MainController@busqueda');

Route::get('/accesibilidad','MainController@accesible');

Route::get('/restaurantes', 'MainController@restaurantes');
Route::get('/salonesbelleza', 'MainController@salonesbelleza');
Route::get('/carnicerias', 'MainController@carnicerias');
Route::get('/droguerias', 'MainController@droguerias');



Route::get('/sivigila', 'MainController@search_for_establecimiento_post');
Route::get('/sivigilav1', 'MainController@search_for_establecimientov1');
Route::get('/sivigilav2', 'MainController@search_for_establecimientov2');
Route::get('/sivigilav3', 'MainController@search_for_establecimientov3');
Route::post('/consultar', 'MainController@search_for_establecimiento_post');

Route::get('/filtroloc', 'MainController@filter_localidad');
Route::get('/filtroupz', 'MainController@filter_upz');

/* *********************** */
// Rutas de administración

Route::get('/admin', 'AdminController@home');
Route::post('/admin', 'AdminController@checklogin');
Route::get('/admin/logout', 'AdminController@logout');
Route::get('/admin/stats','AdminController@stats');

Route::get('/admin/newsletter', 'AdminController@newsletter');
Route::post('/admin/newsletter', 'AdminController@searchnewsletter');

Route::get('/admin/noticias', 'AdminController@noticias');
Route::post('/admin/noticias', 'AdminController@searchnews');
Route::get('/admin/editnoticia/','AdminController@newNoticia');
Route::get('/admin/editnoticia/{id}','AdminController@editNoticia');
Route::get('/admin/tooglenoticia/{id}','AdminController@toogleNoticia');
Route::post('/admin/saveNoticia', 'AdminController@saveNoticia');
Route::get('/admin/nuevanoticia','AdminController@newNoticia');


Route::get('/admin/entradas/',function(){ return redirect(url('/admin')); });
Route::get('/admin/entrada/{id}','AdminController@entradas' );
Route::post('/admin/entradas','AdminController@searchentrada');
Route::get('/admin/editentrada','AdminController@newEntrada');
Route::get('/admin/editentrada/{id}','AdminController@editEntrada');
Route::get('/admin/toogleentrada/{id}','AdminController@toogleEntrada');
Route::post('/admin/saveentrada','AdminController@saveEntrada');
Route::get('/admin/nuevaentrada','AdminController@newEntrada');

Route::get('/admin/descarga','AdminController@download');

Route::get('/download/{file}', 'DownloadsController@download');


//Route::get('/', 'PagesController@index');
Route::post('/export', 'PagesController@export');