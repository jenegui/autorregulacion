<?php

use Illuminate\Database\Seeder;
use App\Models\Noticia;

class NoticiasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Noticia::create([
            'titulo'=>'Secretaría de Salud entrega reconocimiento a 25 restaurantes populares por su calidad y cumplimiento sanitario',
            'resumen'=>'La Secretaría Distrital de Salud (SDS) entregó la distinción "Restaurantes 1A" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.',
            'contenido'=>'La Secretaría Distrital de Salud (SDS) entregó la distinción "Restaurantes 1A" a 25 establecimientos populares de la ciudad que se destacan por cumplir con las normas sanitarias y contar con menús balanceados y variados, la adecuada manipulación de alimentos y buenos niveles de calidad y atención.',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

        Noticia::create([
            'titulo'=>'Recomendaciones para preparar loncheras saludables y nutritivas para el regreso a clases​​',
            'resumen'=>'Con el regreso a los jardines infantiles y a los colegios, los niños y los adolescentes requieren mejorar su concentración para el aprendizaje y reponer la energía que gastan en las actividades diarias. Por esto es importante que cuenten durante la jornada escolar con una lonchera saludable que complemente sus necesidades nutricionales.',
            'contenido'=>'Con el regreso a los jardines infantiles y a los colegios, los niños y los adolescentes requieren mejorar su concentración para el aprendizaje y reponer la energía que gastan en las actividades diarias. Por esto es importante que cuenten durante la jornada escolar con una lonchera saludable que complemente sus necesidades nutricionales.',
            'imagen'=>'noticias/noticia2.jpg',
            'estado'=>'1',
        ]);

    }
}
