<?php

use Illuminate\Database\Seeder;
use App\Models\Entrada;

class EntradaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Entrada::create([
            'enunciado'=>'¿Qué requiero para que me visite la Secretaría Distrital de Salud?',
            'contenido'=>'La Secretaría Distrital de Salud expide el concepto sanitario cuando el establecimiento esté funcionando, a través las Subredes Integradas de Servicios de Salud (SISS), según la localidad donde esté ubicado. Para lo cual, debes haber hecho previamente la Inscripción del establecimiento <a href="./registrate">[Registra tu negocio aquí]</a>, luego verificar que cumples con la norma <a href="./registrate">[Verifica tu lista de chequeo]</a> y después, solicitar una Visita de inspección, vigilancia y control <a href="./registrate">[Solicita una visita]</a> Como resultado de esta vigilancia, se emite un concepto sanitario al establecimiento, acorde a las condiciones sanitarias evidenciadas en el momento de la visita.<br><a href="./registrate">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué reglamentación sanitaria aplica para un restaurante?',
            'contenido'=>'La normatividad sanitaria vigente aplicable para la preparación, expendio, almacenamiento, comercialización de alimentos es: Ley 9 de 1979, Resolución 2674 de 2013 específicamente en el capítulo VIII las condiciones sanitarias que deben cumplir los restaurantes y establecimientos gastronómicos, Resolución 5109 de 2005 y demás normas que rigen sobre la materia.<br><a href="./restaurantes">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué debo saber si tengo o tendré un restaurante?',
            'contenido'=>'Si tienes un restaurante debes cumplir, entre otros, con aspectos como documentación (siempre deberán estar disponibles en el establecimiento el Plan de Saneamiento, el Plan de Capacitación Continuo y Permanente, la Certificación médica del personal); Buenas prácticas de manufactura, infraestructura adecuada (pisos construidos en material sanitario no poroso ni absorbente, de fácil lavado, paredes de tonos claros, con materiales impermeables, techos que eviten la acumulación de suciedad). Los equipos y utensilios deben estar hechos con materiales que no contaminen los alimentos, con acabado liso y sin grietas.<br><a href="./restaurantes">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué requisitos debo cumplir para ser manipulador de alimentos?',
            'contenido'=>'El manipulador de alimentos es toda persona que interviene directamente o en forma ocasional en actividades de fabricación, procesamiento, preparación, envase, almacenamiento, transporte y expendio de alimentos. Debe tener formación en educación sanitaria.<br>Todo manipulador de alimentos debe pasar por un reconocimiento médico antes de desempeñar esta función o cada vez que se considere necesario por razones clínicas y epidemiológicas. Por lo menos una vez al año.<br><a href="./restaurantes">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué son los restaurantes 1A?',
            'contenido'=>'Restaurantes 1A es un programa diseñado para que los restaurantes populares reciban una distinción (válida por un año). El enfoque en calidad e inocuidad de este programa busca brindar a la comunidad alimentos nutritivos, sanos y seguros, con lo cual se contribuye a la salud de la población. Para vincularte a este programa, puedes enviar la solicitud al correo: <a href="mailto:restaurantes1a@saludcapital.gov.co">restaurantes1a@saludcapital.gov.co</a><br><a href="./buenpropietario">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué requisitos existen para la apertura y funcionamiento de un expendio de carnes?',
            'contenido'=>'Los expendios de carnes —conocidos popularmente como carnicerías o famas— son todos los establecimientos que expenden, almacenan y comercializan carne y productos cárnicos comestibles para consumo humano. Algunos de los requisitos sanitarios que deben cumplir los establecimientos son: la carne y los productos cárnicos no deben estar expuestos al medio ambiente; deben contar con indicadores y sistema de registro de temperaturas, y con un sistema de refrigeración con la capacidad de almacenar el volumen de carne que comercializan.<br><a href="./carnicerias">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué permisos requiere un vehículo que transporta carne o derivados cárnicos?',
            'contenido'=>'Los carros o vehículos que transporten carne o productos cárnicos comestibles deben estar autorizados por la Secretaría Distrital de Salud. Debe realizarse una inscripción <a href="http://dev.saludcapital.gov.co/microsivigiladcPruebas/ServiciosComuni2.aspx">[Registra tu vehículo]</a> y, además, solicitar la autorización sanitaria <a href="./registrate">[Solicítala aquí]</a>. <br><br>Todo vehículo debe cumplir con requisitos sanitarios. Para consultarlos, da clic en Más información.<br> <a href="./carnicerias">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué requisitos existen para la apertura y funcionamiento de una droguería?',
            'contenido'=>'De acuerdo con la norma, una droguería debe cumplir, entre otros, con aspectos como instalaciones adecuadas, recurso humano, dotación. Además, debe contar con un sistema de gestión de calidad por procesos.<br><a href="./droguerias">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué requisitos existen para la apertura y funcionamiento de un salón de belleza?',
            'contenido'=>'Las peluquerías, salones de belleza o barberías deben cumplir, entre otros, con requisitos de documentación (diplomas de estudio de cada trabajador, certificación de capacitación en bioseguridad, protocolo/manual de bioseguridad, plan de gestión integral de residuos, así como manuales de instalación, funcionamiento y registro de mantenimiento periódico de equipos). En el establecimiento, los pisos, paredes, techos, escaleras, rampas y divisiones deben estar construidos o recubiertos con pinturas o materiales sanitarios de tonos claros. El mobiliario construido debe estar recubierto o tapizado en material sanitario.<br><a href="./salonesbelleza">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);
        Entrada::create([
            'enunciado'=>'¿Qué requisitos se exigen para abrir un hogar geriátrico en Bogotá D.C.?',
            'contenido'=>'Para su correcto funcionamiento, los hogares geriátricos, gerontológicos y centros día deben cumplir con los siguientes requisitos: contar con profesionales de salud idóneos, calificados, preparados y amables para la atención de la persona mayor, lo cual se debe garantizar permanentemente en las instalaciones; garantizar una alimentación adecuada y saludable, acorde con las necesidades de salud de la persona institucionalizada; mantener buenas prácticas de manejo de alimentos, y contar con una infraestructura adecuada para su desplazamiento.<br><a href="./registrate">Más información</a>',
            'tipo'=>'1',
            'estado'=>'1'
        ]);












        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Recuerda: el registro o inscripción de establecimientos es obligatorio para todo tipo de negocio.',
            'tipo'=>'2',
            'archivo'=>'img/icons/tipsauto1.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'El cumplimiento de los requisitos sanitarios vigentes brinda mayor confianza a tus clientes, lo cual conlleva a n incremento de tus ingresos por los servicios brindados en el establecimiento.',
            'tipo'=>'2',
            'archivo'=>'img/icons/tipsauto2.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Si realizas la autoevaluación en el proceso de autorregulación, conocerás requisitos para el funcionamiento de los establecimientos y apuntarás a contar con un concepto sanitario favorable.',
            'tipo'=>'2',
            'archivo'=>'img/icons/tipsauto3.svg',
            'estado'=>'1'
        ]);

        Entrada::create([
            'enunciado'=>'',
            'contenido'=>'Algunas de las normas que regulan el proceso de registro de establecimientos son: Resolución 1229 de 2013, Resolución 5194 de 2010, Resolución 2263 de 2004, Resolución No. 2016041871 de 2016.',
            'tipo'=>'2',
            'archivo'=>'img/icons/tipsauto4.svg',
            'estado'=>'1'
        ]);
   }
}
